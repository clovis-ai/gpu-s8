FROM ubuntu:latest

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update && \
	apt-get install -y opencl-headers \
	libsdl2-dev libsdl2-image-dev libsdl2-ttf-dev \
	ocl-icd-opencl-dev libhwloc-dev libfxt-dev libopenmpi3 flex \
	cmake git \
	&& rm -rf /var/lib/apt/lists/*

RUN ln -s /usr/lib/x86_64-linux-gnu/libfxt.so.1 /usr/lib/x86_64-linux-gnu/libfxt.so.0
